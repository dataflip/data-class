<?php

namespace DataBackFlip;

use Exception;
use PDO;
use Throwable;

class Data
{
    //todo check for columns existence before insert,edit,update
    //todo debate between check existence of tables and columns each time, probably not.
    //todo run tests on alter commands
    public PDO $dataHost;
    public string $dbName = '';
    public int|null $count;
    public array|null $result;
    public array $row;
    public mixed $lastInsertId;
    public int|null $affectedRows;
    public array|null $allTableDetails = [];
    public array|null $allTableNames = [];
    public array|null $primaryKeys = [];
    public array|null $misc = [];
    public static array $fieldTypes = ['TINYINT' => 'TINYINT', 'SMALLINT' => 'SMALLINT', 'MEDIUMINT' => 'MEDIUMINT', 'INT' => 'INT', 'BIGINT' => 'BIGINT', 'FLOAT' => 'FLOAT', 'DOUBLE' => 'DOUBLE', 'DECIMAL' => 'DECIMAL', 'BIT' => 'BIT', 'CHAR' => 'CHAR', 'VARCHAR' => 'VARCHAR', 'TINYTEXT' => 'TINYTEXT', 'TEXT' => 'TEXT', 'MEDIUMTEXT' => 'MEDIUMTEXT', 'LONGTEXT' => 'LONGTEXT', 'BINARY' => 'BINARY', 'VARBINARY' => 'VARBINARY', 'TINYBLOB' => 'TINYBLOB', 'BLOB' => 'BLOB', 'MEDIUMBLOB' => 'MEDIUMBLOB', 'LONGBLOB' => 'LONGBLOB', 'ENUM' => 'ENUM', 'SET' => 'SET', 'DATE' => 'DATE', 'DATETIME' => 'DATETIME', 'TIME' => 'TIME', 'TIMESTAMP' => 'TIMESTAMP', 'YEAR' => 'YEAR'];
    public static array $searchConjunctions = ['$and' => ' AND ', '$or' => ' OR ', '$parenthesis' => ' ('];
    public static array $searchOperatorsStart = [
        '$eq' => ' = ', '$neq' => ' != ', '$lt' => ' < ', '$lte' => ' <= ', '$gt' => ' > ', '$gte' => ' >= ',
        '$in' => ' IN(', '$nin' => ' NOT IN(', '$contains' => " LIKE CONCAT('%',", '$ncontains' => " NOT LIKE CONCAT('%',",
        '$endw' => " LIKE CONCAT('%',", '$nendw' => " NOT LIKE CONCAT('%',", '$bgw' => ' LIKE CONCAT(', '$nbgw' => ' NOT LIKE CONCAT(',
        '$null' => ' IS NULL ', '$nnull' => ' IS NOT NULL ', '$empty' => " = '' ", '$nempty' => " != '' ", '$btwn' => ' BETWEEN ',
        '$nbtwn' => ' NOT BETWEEN ', '$true' => ' IS TRUE ', '$ntrue' => ' IS NOT TRUE ', '$false ' => ' IS FALSE ', '$nfalse' => ' IS NOT FALSE '
    ];
    public static array $searchNullableOperators = ['$null', '$nnull', '$empty', '$nempty', '$true', '$ntrue', '$false', '$nfalse'];
    public static array $searchOperatorsEnd = [
        '$eq' => '', '$neq' => '', '$lt' => '', '$lte' => '', '$gt' => '', '$gte' => '', '$in' => ')',
        '$nin' => ')', '$contains' => ",'%')", '$ncontains' => ",'%')", '$endw' => ')', '$nendw' => ')',
        '$bgw' => ",'%')", '$nbgw' => ",'%')", '$null' => '', '$nnull' => '', '$empty' => '', '$nempty' => '',
        '$btwn' => '', '$nbtwn' => '', '$true' => '', '$ntrue' => '', '$false' => '', '$nfalse' => ''
    ];
    public static array $dateUnits = [
        '$microsecond' => 'MICROSECOND',
        '$second' => 'SECOND',
        '$minute' => 'MINUTE',
        '$hour' => 'HOUR',
        '$day' => 'DAY',
        '$week' => 'WEEK',
        '$month' => 'MONTH',
        '$year' => 'YEAR',
    ];
    public static string $databaseIdentifier = 'Database';
    public static string $tableIdentifier = 'Table';
    public static string $pivotTableIdentifier = 'Pivot Table';
    public static string $columnIdentifier = 'Column';
    public static string $indexIdentifier = 'Index';
    public static string $constraintIdentifier = 'Constraint';
    public static string $storedProgramIdentifier = 'Stored Program';
    public static string $viewIdentifier = 'View';
    public static string $tablespaceIdentifier = 'Tablespace';
    public static string $serverIdentifier = 'Server';
    public static string $logFileGroupIdentifier = 'Log File Group';
    public static string $aliasIdentifier = 'Alias';
    public static string $compoundStatementLabelIdentifier = 'Compound Statement Label';
    public static string $userDefinedVariableIdentifier = 'User-Defined Variable';
    public static string $resourceGroupIdentifier = 'Resource Group';
    public static string $jsonDecode = 'json_decode';

    /**
     * checks database connection and sets database and start up variables
     * @param string $setDb - database index|key name. Default is default
     * @param array|null $currentDatabase - current database array. If not provided, the current database is set based on the $setDb key|index name
     * @param string $dbFileLocation - file name where the database configurations are stored
     * @throws Exception
     */
    public function __construct(string $setDb = '', ?array $currentDatabase = null, string $dbFileLocation = '/aaa-config/general/database.php')
    {
        try {
            if (is_null($currentDatabase) || count($currentDatabase) == 0) {
                $dbInfo = [];
                $dbFile = $_SERVER['DOCUMENT_ROOT'] . $dbFileLocation;
                if (file_exists($dbFile)) {
                    include($dbFile);
                    if (count($dbInfo) == 0) {
                        throw new Exception('Database config file error', 401);
                    }
                } else {
                    throw new Exception('Database config file not present', 401);
                }

                if (empty($setDb)) {
                    if ($dbInfo['default']) {
                        $setDb = 'default';
                    } else {
                        throw new Exception('Default database not provided', 401);
                    }
                }

                $databasePresent = 'no';
                $currentDatabase = [];
                if (array_key_exists($setDb, $dbInfo) && is_array($dbInfo[$setDb])) {
                    $databasePresent = 'yes';
                    $currentDatabase = $dbInfo[$setDb];
                }
            } else {
                $databasePresent = 'yes';
            }

            if ($databasePresent != 'yes' && is_array($currentDatabase)) {
                throw new Exception('Database not provided', 401);
            } elseif (isset($currentDatabase['host']) && isset($currentDatabase['username']) && isset($currentDatabase['password']) && isset($currentDatabase['database'])) {
                $host = $currentDatabase['host'];
                $username = $currentDatabase['username'];
                $password = $currentDatabase['password'];
                $database = $currentDatabase['database'];
                $this->dbName = $currentDatabase['database'];
                if (empty($currentDatabase['charset'])) {
                    $charset = 'utf8';
                } else {
                    $charset = $currentDatabase['charset'];
                }
                $pdoOptions = [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $charset,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ];

                if (version_compare(PHP_VERSION, '8.1', '>=')) {
                    $pdoOptions[PDO::ATTR_STRINGIFY_FETCHES] = true;
                }
                $this->dataHost = new PDO(
                    "mysql:host=$host; dbname=$database",
                    $username,
                    $password,
                    $pdoOptions
                );
                $this->getAllTableDetails($this->dbName);
            } else {
                throw new Exception('Database configurations not provided', 401);
            }
        } catch (Throwable $th) {
            if (isset($_GET['testdb'])) {
                print_r($th);
            }

            throw new Exception('Database connection error', 401);
        }
    }

    /**
     * validate table and column names
     * @param string $name - name of the table or column
     * @return bool
     * @throws Exception
     */
    public static function checkCharactersTableAndFieldNames(string $name): bool
    {
        if (!preg_match('/^[a-z_\d]+$/i', $name)) {
            throw new Exception('Has bad characters', 400);
        }

        if (is_numeric($name)) {
            throw new Exception('Name is numeric only', 400);
        }

        if (preg_match('/^[_\d]+$/i', $name)) {
            throw new Exception('Only has underscores', 400);
        }

        return true;
    }

    /**
     * @param string $name
     * @return bool
     * @throws Exception
     */
    public static function checkValidNameCharacters(string $name): bool
    {
        if (!preg_match('/^[a-z_\-\d]+$/i', $name)) {
            throw new Exception($name . 'Has bad characters', 400);
        }

        return true;
    }

    /**
     * validate table name then verify if the table exists in the database.
     * This is a MYSQL specific solution
     * @param string $table - table name
     * @param bool $exception - throw exception if table is not found
     * @param string|null $dbName
     * @return bool
     * @throws Exception
     */
    public function checkIfTableExists(string $table, bool $exception = false, ?string $dbName = null): bool
    {
        self::checkCharactersTableAndFieldNames($table);
        $this->getRows('SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = ? AND table_name = ?', [$this->setDbName($dbName), $table]);
        if ($this->count != 1) {
            if ($exception) {
                throw new Exception("Table $table does not exist", 404);
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * verify is a column exists in a table by first checking if the table exists in the database
     * This is a MYSQL specific solution
     * @param string $table - table name
     * @param string $column - column name
     * @param bool $exception - throw exception if column is not found
     * @param string|null $dbName
     * @return bool
     * @throws Exception
     */
    public function checkIfColumnInTableExists(string $table, string $column, bool $exception = false, ?string $dbName = null): bool
    {
        self::checkCharactersTableAndFieldNames($table);
        self::checkCharactersTableAndFieldNames($column);
        $this->getRows('SELECT * FROM information_schema.columns WHERE table_schema = ? AND table_name = ? AND column_name = ?', [$this->setDbName($dbName), $table, $column]);
        if ($this->count != 1) {
            if ($exception) {
                throw new Exception('Column does not exist', 404);
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * return all columns in the specified table by verifying if the given table exists in the database
     * @param string $table - table name
     * @param bool $includeDefaults
     * @param string|null $dbName
     * @param bool $returnAll
     * @return bool|array
     * @throws Exception
     */
    public function getColumns(string $table, bool $includeDefaults = false, ?string $dbName = null, bool $returnAll = false): bool|array
    {
        if (self::checkCharactersTableAndFieldNames($table)) {
            $dbNamePrefix = $this->setDbNamePrefix($dbName);
            $columns = $this->dataHost->prepare("DESCRIBE $dbNamePrefix$table");
            $columns->execute();
            if ($returnAll) {
                $columnsInfo = $columns->fetchAll(PDO::FETCH_ASSOC);

                return array_combine(array_column($columnsInfo, 'Field'), $columnsInfo);
            }

            if ($includeDefaults) {
                return array_column($columns->fetchAll(PDO::FETCH_ASSOC), 'Default', 'Field');
            }

            return ($columns->fetchAll(PDO::FETCH_COLUMN));
        } else {
            return false;
        }
    }

    /**
     * return all table information
     * @return array|null ; in $this->allTableNames and $this->allTableDetails
     * @throws Throwable
     */
    public function getAllTableDetails(?string $dbName = null): ?array
    {
        $this->primaryKeys = [];
        $this->allTableNames = [];
        $this->allTableDetails = [];
        foreach ($this->getRows('SELECT * FROM information_schema.columns WHERE table_schema = ?', [$this->setDbName($dbName)]) as $row) {
            if ($row['COLUMN_KEY'] == 'PRI') {
                $primary = 'yes';
                $this->primaryKeys[$row['TABLE_NAME']] = $row['COLUMN_NAME'];
            } else {
                $primary = '';
            }

            $this->allTableNames[] = $row['TABLE_NAME'];
            $this->allTableDetails[$row['TABLE_NAME']][$row['COLUMN_NAME']] = [
                'primary' => $primary,
                'type' => $row['DATA_TYPE'],
                'maxCharacters' => $row['CHARACTER_MAXIMUM_LENGTH'],
                'isNullable' => $row['IS_NULLABLE'],
                'charset' => $row['CHARACTER_SET_NAME'],
                'collation' => $row['COLLATION_NAME'],
                'extra' => $row['EXTRA'],
                'ellaboratedType' => $row['COLUMN_TYPE']
            ];
        }

        return $this->allTableDetails;
    }

    /**
     * @throws Exception
     */
    public function getTableDetails(array $tableNames = [], ?string $dbName = null): ?array
    {
        $allTableDetails = [];
        $whereInfoTbl = '';
        $whereFkTbl = '';
        $params = [$this->setDbName($dbName)];
        $tableNames = array_unique($tableNames);
        $totalTableNames = count($tableNames);
        if ($totalTableNames > 0) {
            $whereTbl = 'TABLE_NAME IN(?' . str_repeat(',?', $totalTableNames - 1) . ')';
            $whereInfoTbl .= " AND $whereTbl";
            $whereFkTbl .= " AND kcu.$whereTbl";
            $params = array_merge($params, $tableNames);
        }

        $tableInfo = array_column($this->getRows("SELECT TABLE_NAME,JSON_ARRAYAGG(JSON_OBJECT('COLUMN_NAME', COLUMN_NAME, 'DATA_TYPE', DATA_TYPE, 
            'IS_NULLABLE', IS_NULLABLE,'COLUMN_KEY', COLUMN_KEY, 'CHARACTER_MAXIMUM_LENGTH', CHARACTER_MAXIMUM_LENGTH,
            'CHARACTER_SET_NAME', CHARACTER_SET_NAME, 'COLLATION_NAME', COLLATION_NAME, 'EXTRA', EXTRA, 'COLUMN_TYPE', COLUMN_TYPE,'COLUMN_DEFAULT',COLUMN_DEFAULT,'ENUM',IF(DATA_TYPE = 'enum', CONCAT(CONCAT('[\"',TRIM('\'' FROM REPLACE(REPLACE(REPLACE(COLUMN_TYPE, 'enum(', ''), ')', ''),'\',\'','\",\"'))),'\"]'), JSON_ARRAY()))) COLUMNS
            FROM information_schema.columns 
            WHERE table_schema = ? $whereInfoTbl 
            GROUP BY TABLE_NAME", $params, [$this::$jsonDecode => true]) ?? [], 'COLUMNS', 'TABLE_NAME');
        if ($totalTableNames > 0 && $totalTableNames > count($tableInfo)) {
            throw new Exception('Invalid Table(s)', 403);
        }

        $foreignKeys = array_column($this->getRows("SELECT kcu.TABLE_NAME,JSON_ARRAYAGG(
            JSON_OBJECT('TABLE_SCHEMA',kcu.TABLE_SCHEMA,'TABLE_NAME', kcu.TABLE_NAME, 'COLUMN_NAME', kcu.COLUMN_NAME, 'CONSTRAINT_NAME',kcu.CONSTRAINT_NAME, 
                'REFERENCED_TABLE_NAME', kcu.REFERENCED_TABLE_NAME,'REFERENCED_COLUMN_NAME', kcu.REFERENCED_COLUMN_NAME, 'DELETE_RULE', rc.DELETE_RULE, 'UPDATE_RULE', rc.UPDATE_RULE)) FOREIGN_KEY
            FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
            INNER JOIN  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc ON kcu.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
            WHERE kcu.REFERENCED_TABLE_SCHEMA = ? $whereFkTbl 
            GROUP BY kcu.TABLE_NAME", $params, [$this::$jsonDecode => true]) ?? [], 'FOREIGN_KEY', 'TABLE_NAME');
        foreach ($tableInfo as $tableName => $info) {
            $primaryKeys = [];
            foreach ($info as $row) {
                if ($row['COLUMN_KEY'] == 'PRI') {
                    $primaryKeys[] = $row['COLUMN_NAME'];
                }

                $row['ENUM'] = json_decode($row['ENUM'], true);
                $allTableDetails[$tableName]['columns'][] = $row;
            }

            $allTableDetails[$tableName]['primary_keys'] = $primaryKeys;
            $allTableDetails[$tableName]['foreign_keys'] = (empty($foreignKeys[$tableName]) ? [] : $foreignKeys[$tableName]);
        }

        return $allTableDetails;
    }

    /**
     * resets all global variables belonging to this class
     */
    public function reset(): void
    {
        $this->row = [];
        $this->result = [];
        $this->count = NULL;
        $this->lastInsertId = NULL;
        $this->affectedRows = NULL;
    }

    /**
     * return the primary key of a given table by first verifying if the table exists in the database
     * @param string $table - table name
     * @param bool $exception - throw exception if key is not found
     * @param bool $isAutoIncrement - check if primary key is auto increment
     * @param string|null $dbName
     * @param bool $returnArray
     * @return string|array primary key
     * @throws Exception
     */
    public function getPrimaryKey(string $table, bool $exception = false, bool $isAutoIncrement = false, ?string $dbName = null, bool $returnArray = false): string|array
    {
        $this->checkIfTableExists($table, true, $dbName);
        $primary = [];
        $query = 'SELECT DISTINCT tc.TABLE_NAME,tc.CONSTRAINT_TYPE ,kcu.COLUMN_NAME,UPPER(c.EXTRA) AS EXTRA
                  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
                  INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu ON tc.CONSTRAINT_SCHEMA = kcu.CONSTRAINT_SCHEMA AND tc.CONSTRAINT_NAME = kcu.CONSTRAINT_NAME AND tc.TABLE_NAME = kcu.TABLE_NAME
                  INNER JOIN INFORMATION_SCHEMA.COLUMNS c ON kcu.TABLE_NAME=c.TABLE_NAME AND kcu.COLUMN_NAME=c.COLUMN_NAME
                  WHERE tc.CONSTRAINT_SCHEMA = ? AND tc.TABLE_NAME = ? AND tc.CONSTRAINT_TYPE=?';
        $dbQuery = $this->dataHost->prepare($query);
        $dbQuery->execute([$this->setDbName($dbName), $table, 'PRIMARY KEY']);
        foreach ($dbQuery->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $primary[$row['COLUMN_NAME']] = $row['COLUMN_NAME'];
            if ($isAutoIncrement && $row['EXTRA'] !== 'AUTO_INCREMENT') {
                $primary = [];
                if ($exception) {
                    throw new Exception('No auto increment key', 404);
                }
            }
        }

        if (empty($primary) && $exception) {
            throw new Exception('No primary key', 404);
        }

        return $returnArray ? $primary : implode(',', $primary);
    }

    /**
     * return records for a custom query
     * @param string $query - custom query
     * @param array $params - parameters used in preparing the query
     * @param array $options
     * @return array|null
     * @throws Exception
     */
    public function getRows(string $query, array $params = [], array $options = []): ?array
    {
        if (empty($query)) {
            throw new Exception('No query', 400);
        }

        $this->reset();
        $dbQuery = $this->dataHost->prepare($query);
        $dbQuery->execute($params);
        $this->result = $dbQuery->fetchAll(PDO::FETCH_ASSOC);
        $this->count = count($this->result);
        if ($this->count == 1) {
            foreach ($this->result as $row) {
                $this->row = $row;
            }
        }

        $response = $this->result;
        if (empty($options[$this::$jsonDecode]) || !is_bool($options[$this::$jsonDecode])) {
            return $response;
        }

        $this::json_decode_recursively($response);
        $this::json_decode_recursively($this->row);
        return $response;
    }

    /**
     * return all records for a given table
     * @param string $table - table name
     * @param array $orderBy - @format {"column_name1":"asc", "column_name2":"desc"}
     * @param int $limit - number of records to be returned. Default 0 means return all
     * @param int $offset - number of records to skip
     * @param string|null $dbName
     * @return array|null
     * @throws Exception
     */
    public function getAllRows(string $table, array $orderBy = [], int $limit = 0, int $offset = 0, ?string $dbName = null): ?array
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->checkIfTableExists($table, true, $dbName);
        $query = "SELECT * FROM $dbNamePrefix$table " . $this->setOrderBy($table, $orderBy, $dbName);
        if ($limit > 0) {
            $query .= ' LIMIT ';
            if ($offset > 0) {
                $query .= $offset . ',';
            }

            $query .= $limit;
        }

        return $this->getRows($query);
    }

    /**
     * execute a query that doesn't expect a database response on success
     * @param string $query - the query to be executed
     * @param array $params - parameters used in preparing the query
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Exception
     */
    public function execute(string $query, array $params = [], bool $queryPreviewOnly = false): bool|string
    {
        if (empty($query)) {
            throw new Exception('No query', 400);
        }

        $this->reset();
        $this->reset();
        $dbQuery = $this->dataHost->prepare($query);
        $query = $dbQuery->queryString;
        $tQuery = $dbQuery->queryString;
        foreach ($params as $key => $value) {
            if (is_numeric($key)) {
                $tKey = $key + 1;
                $isNamedParam = false;
            } else {
                $tKey = ":$key";
                $isNamedParam = true;
            }

            $v = $this->formatQueryParam($value);
            $dbQuery->bindValue($tKey, $value);
            if ($isNamedParam) {
                $tQuery = preg_replace("/$tKey\b/", $v, $tQuery);
            } else {
                $tQuery = preg_replace('/\?/', $v, $tQuery, 1);
            }

        }

        if ($queryPreviewOnly) {
            $this->validateQueryParams($params, $query);

            return $tQuery;
        }

        $dbQuery->execute();
        $this->result = $dbQuery->fetchAll(PDO::FETCH_ASSOC);
        if ($this->result) {
            $this->count = count($this->result);
            $this->lastInsertId = $this->dataHost->lastInsertId();
            $this->affectedRows = $dbQuery->rowCount();
            if ($this->count == 1) {
                foreach ($this->result as $row) {
                    $this->row = $row;
                }
            }
        }

        return true;
    }

    /**
     * save one record to the database ignoring invalid data that might cause SQL error
     * @param string $table - table name
     * @param array $params - parameters used in preparing the query
     * @param bool $updateDuplicates - on duplicate values update
     * @param string|null $primaryKey
     * @param bool $ignoreInvalidColumns - if set to true, checks if the columns exists first and inserts to only valid columns
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return mixed  primary_key
     * @throws Throwable
     */
    public function insertRecord(string $table, array $params, bool $updateDuplicates = FALSE, ?string $primaryKey = NULL, bool $ignoreInvalidColumns = false, ?string $dbName = null, bool $queryPreviewOnly = false): mixed
    {
        if (count($params) == 0) {
            throw new Exception('No valid columns', 400);
        }

        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->checkIfTableExists($table, !$queryPreviewOnly, $dbName);
        $this->reset();
        $columns = '';
        $valString = '';
        $duplicateString = '';
        if (!$queryPreviewOnly && $ignoreInvalidColumns) {
            $params = $this->setUpsertFields($table, $params, $dbName);
        }

        foreach ($params as $key => $val) {
            self::checkCharactersTableAndFieldNames($key);
            if ($columns != '') {
                $columns .= ',';
            }

            $columns .= "`$key`";
            $duplicateString .= "`$key` = new.`$key`, ";
            if ($valString != '') {
                $valString .= ',';
            }

            $valString .= ':' . $key;
        }

        $duplicateString = rtrim($duplicateString, ', ');
        $lastInsertId = NULL;
        if ($updateDuplicates) {
            $query = 'INSERT INTO ' . $dbNamePrefix . $table . ' ( ' . $columns . ' ) VALUES( ' . $valString . " ) AS new ON DUPLICATE KEY UPDATE $duplicateString";
            $autoIncrement = $primaryKey ?? $this->getPrimaryKey($table, false, true, $dbName);
            if (!empty($autoIncrement)) {
                if (in_array($autoIncrement, array_keys($params))) {
                    $lastInsertId = $params[$autoIncrement];
                } else {
                    $query .= ",$autoIncrement = LAST_INSERT_ID($autoIncrement)";
                }
            }
        } else {
            $query = 'INSERT INTO ' . $dbNamePrefix . $table . ' ( ' . $columns . ' ) VALUES( ' . $valString . ' )';
        }

        $this->misc['query'] = $query;
        $this->misc['params'] = $params;
        $dbQuery = $this->dataHost->prepare($query);
        $query = $dbQuery->queryString;
        $tQuery = $dbQuery->queryString;
        foreach ($params as $key => $value) {
            $tKey = ":$key";
            $v = $this->formatQueryParam($value);
            $dbQuery->bindValue($tKey, $value);
            $tQuery = preg_replace("/$tKey\b/", $v, $tQuery);
        }

        if ($queryPreviewOnly) {
            $this->validateQueryParams($params, $query);

            return $tQuery;
        }

        $dbQuery->execute();
        $this->lastInsertId = empty($lastInsertId) ? $this->dataHost->lastInsertId() : $lastInsertId;
        $this->misc['lastInsert'] = $this->lastInsertId;
        $this->affectedRows = $dbQuery->rowCount();
        $this->misc['affected'] = $this->affectedRows;

        return $this->lastInsertId;
    }

    /**
     * save multiple records into the database, replacing existing values in case of duplicates
     * This does not ignore invalid data; instead, it throws an error in such an instance
     * @param string $table - table name
     * @param array $params - parameters used in preparing the query
     * @param bool $ignoreInvalidColumns
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|array
     * @throws Exception
     */
    public function insertMultipleRecords(string $table, array $params, bool $ignoreInvalidColumns = false, ?string $dbName = null, bool $queryPreviewOnly = false): bool|array
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->checkIfTableExists($table, !$queryPreviewOnly, $dbName);
        if (count($params) == 0) {
            throw new Exception('Missing Column Values', 400);
        }

        $this->reset();
        $notNullColumns = $this->getNotNullNoDefaultColumns($table, $dbName);
        $primaryKeys = $this->getPrimaryKey($table, false, false, $dbName, true);
        $pkKey = "CONCAT_WS('--'," . implode(',', $primaryKeys) . ')';
        $recordsByPkKey = '';
        $recordsByPk = [];
        if (!$queryPreviewOnly || ($this->checkIfTableExists($table))) {
            $validColumns = $this->getColumns($table, false, $dbName);
            $allColumnsValid = false;
        } else {
            $validColumns = [];
            $allColumnsValid = true;
        }

        $groupedParams = [];
        foreach ($params as $param) {
            $initialKeys = array_keys($param);
            if ($allColumnsValid) {
                $validColumns = array_merge($validColumns, $initialKeys);
            }

            if (!$queryPreviewOnly && !$ignoreInvalidColumns) {
                $invalidColumns = array_diff($initialKeys, $validColumns);
                if (count($invalidColumns) > 0) {
                    throw new Exception('Invalid Column(s) ' . implode(',', $invalidColumns), 400);
                }
            }

            $param = array_filter($param, function ($v, $k) use ($validColumns) {
                return in_array($k, $validColumns);
            }, ARRAY_FILTER_USE_BOTH);
            ksort($param);
            $keys = array_keys($param);
            if (count($keys) == 0) {
                throw new Exception('No valid columns amongst ' . implode(',', $initialKeys), 400);
            }

            $initialKeys = $keys;
            $pkExists = array_reduce($primaryKeys, function ($carry, $col) use ($param) {
                return $carry && array_key_exists($col, $param) && $param[$col] !== null;
            }, true);
            $missingNotNullColumns = array_filter($notNullColumns, function ($col) use ($param) {
                return !array_key_exists($col, $param) || $param[$col] === null;
            });

            $key = md5(implode(',', $keys));
            if ($pkExists && count($missingNotNullColumns) > 0) {
                $initialKeys = array_values(array_unique(array_merge($initialKeys, $missingNotNullColumns)));
                sort($initialKeys);
                $key = md5(implode(',', $initialKeys));
                $recordsByPk[$key] ??= [];
                $groupedParams[$key] ??= ['columns' => [], 'params' => []];
                $dataValue = implode('--', array_map(fn($col) => $param[$col], $primaryKeys));
                $groupedParamKey = count($groupedParams[$key]['params']);
                $recordsByPk[$key][$groupedParamKey] = [
                    'missingNotNullColumns' => $missingNotNullColumns,
                    'dataValue' => $dataValue
                ];
                $recordsByPkKey .= "WHEN $pkKey = $dataValue THEN $groupedParamKey ";
            }

            $sortedParam = [];
            foreach ($initialKeys as $k) {
                $sortedParam[$k] = $param[$k] ?? null;
            }

            $param = $sortedParam;
            $groupedParams[$key] ??= ['columns' => [], 'params' => []];
            $groupedParams[$key]['params'][] = $param;
            $groupedParams[$key]['columns'] = $initialKeys;
        }

        $missingNotNullValues = [];
        if (!empty($recordsByPk)) {
            $missingNotNullParams = [];
            foreach ($recordsByPk as $pkGroup) {
                foreach ($pkGroup as $record) {
                    $missingNotNullParams[] = $record['dataValue'];
                }
            }

            $tMissingNotNullValues = $this->getRows('SELECT ' . implode(',', $notNullColumns) . ", CASE $recordsByPkKey END AS grouped_param_key FROM $table WHERE $pkKey IN (?" . str_repeat(',?', count($missingNotNullParams) - 1) . ')', $missingNotNullParams);
            $missingNotNullValues = array_reduce($tMissingNotNullValues, function ($acc, $item) {
                $acc[$item['grouped_param_key']] = $item;
                return $acc;
            }, []);
        }


        $tQueries = [];
        foreach ($groupedParams as $j => $groupedParam) {
            if (array_key_exists($j, $recordsByPk)) {
                foreach ($recordsByPk[$j] as $x => $record) {
                    if (array_key_exists($x, $recordsByPk[$j]) && array_key_exists($x, $groupedParam['params'])) {
                        foreach ($record['missingNotNullColumns'] as $column) {
                            $groupedParam['params'][$x][$column] = $missingNotNullValues[$x][$column] ?? null;
                        }
                    }
                }
            }

            $params = $groupedParam['params'];
            $columns = $groupedParam['columns'];
            $duplicates = [];
            foreach ($columns as $column) {
                $duplicates[] = "$column = new.$column";
            }

            $duplicateString = implode(',', $duplicates);
            $valString = '';
            for ($i = 0; $i < count($params); $i++) {
                $valString .= '(:' . implode($i . ', :', $columns) . $i . '),';
            }

            $valString = rtrim($valString, ', ');
            $duplicateString = rtrim($duplicateString, ', ');
            $query = "INSERT INTO $dbNamePrefix$table (" . implode(', ', $columns) . ") VALUES$valString AS new ON DUPLICATE KEY UPDATE $duplicateString";
            $dbQuery = $this->dataHost->prepare($query);
            $query = $dbQuery->queryString;
            $tQuery = $dbQuery->queryString;
            $count = 0;
            $queryParams = [];
            foreach ($params as $paramArr) {
                foreach ($paramArr as $column => $value) {
                    if (in_array($column, $columns)) {
                        $tKey = ":$column$count";
                        $v = $this->formatQueryParam($value);
                        $dbQuery->bindValue($tKey, $value);
                        $queryParams[$tKey] = $value;
                        $tQuery = preg_replace("/$tKey\b/", $v, $tQuery);
                    }
                }

                $count++;
            }

            if ($queryPreviewOnly) {
                $this->validateQueryParams($queryParams, $query);
                $tQueries[] = $tQuery;

                continue;
            }

            $dbQuery->execute();
        }

        return $queryPreviewOnly ? $tQueries : true;
    }

    /**
     * update a database record based on the primary key only
     * This does not ignore invalid data; instead, it throws an error in such an instance
     * @param string $table - table name
     * @param mixed $record -  the primary key record value
     * @param array $params - parameters used in preparing the query
     * @param string|null $primaryKey
     * @param bool $ignoreInvalidColumns - if set to true, checks if the columns exists first and inserts to only valid columns
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Throwable
     */
    public function updateRecord(string $table, mixed $record, array $params, ?string $primaryKey = NULL, bool $ignoreInvalidColumns = false, ?string $dbName = null, bool $queryPreviewOnly = false): bool|string
    {
        if (count($params) == 0) {
            return true;
        }

        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->checkIfTableExists($table, true, $dbName);
        $this->reset();
        $primaryKey = $primaryKey ?? $this->getPrimaryKey($table, true, false, $dbName);
        $query = 'UPDATE ' . $dbNamePrefix . $table . ' SET ';
        $queryArray = [];
        $updateParams = [];
        if ($ignoreInvalidColumns) {
            $params = $this->setUpsertFields($table, $params, $dbName);
        }

        foreach ($params as $key => $value) {
            self::checkCharactersTableAndFieldNames($key);
            $queryArray[] = "`$key`=?";
            $updateParams[] = $value;
        }

        $query .= implode(',', $queryArray);
        $updateParams[] = $record;
        $query .= ' WHERE ' . $primaryKey . '=?';
        $dbQuery = $this->dataHost->prepare($query);
        $query = $dbQuery->queryString;
        $tQuery = $dbQuery->queryString;
        foreach ($updateParams as &$value) {
            $v = $this->formatQueryParam($value);
            $tQuery = preg_replace('/\?/', $v, $tQuery, 1);
        }

        if ($queryPreviewOnly) {
            $this->validateQueryParams($updateParams, $query);

            return $tQuery;
        }

        $dbQuery->execute($updateParams);
        $this->affectedRows = $dbQuery->rowCount();

        return true;
    }

    /**
     * update a database record based on the primary key and other conditions, ignoring invalid data that might cause SQL error
     * @param string $table - table name
     * @param mixed $record -  the primary key record value
     * @param array $params - parameters used in preparing the query
     * @param array $conditions - other query conditions used in updating the record
     * @param string $conjunction - AND|OR to join the primary key record condition and other conditions
     * @return bool|int|null  number of records updated
     * @throws Throwable
     */
    public function updateRecord_conditions_with_record(string $table, mixed $record, array $params, array $conditions, string $conjunction = 'AND', ?string $primaryKey = NULL, ?string $dbName = null): bool|int|null
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        if ($this->checkIfTableExists($table, true, $dbName) && count($params) > 0 && count($conditions) > 0) {
            $this->reset();
            $primaryKey = $primaryKey ?? $this->getPrimaryKey($table, true, false, $dbName);
            if (!empty($primaryKey)) {
                $conditionString = '';
                $query = 'UPDATE IGNORE ' . $dbNamePrefix . $table . ' SET ';
                $queryArray = [];
                $updateParams = [];
                foreach ($params as $key => $val) {
                    if (self::checkCharactersTableAndFieldNames($key)) {
                        $queryArray[] = $key . '=?';
                        $updateParams[] = $val;
                    } else {
                        return false;
                    }
                }

                $query .= implode(',', $queryArray);
                foreach ($conditions as $key => $val) {
                    if (self::checkCharactersTableAndFieldNames($key)) {
                        $updateParams[] = $val;
                        if (!empty($conditionString)) {
                            $conditionString .= ' ' . $conjunction . ' ';
                        }

                        $conditionString .= $key . '=?';
                    } else {
                        return false;
                    }
                }

                $query .= ' WHERE ' . '(' . $conditionString . ')';
                $query .= ' AND ' . $primaryKey . '=?';
                $updateParams[] = $record;
                $dbQuery = $this->dataHost->prepare($query);
                $dbQuery->execute($updateParams);
                $this->affectedRows = $dbQuery->rowCount();

                return $this->affectedRows;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * update a database based on given conditions, ignoring invalid data that might cause SQL error
     * @param string $table - table name
     * @param array $params - parameters used in preparing the query
     * @param array $conditions - other query conditions used in updating the record
     * @param string $conjunction - AND|OR to join the given conditions
     * @return bool|int|null  number of records updated
     * @throws Throwable
     */
    public function updateRecords_conditions_no_record(string $table, array $params, array $conditions, string $conjunction = 'AND', ?string $dbName = null): bool|int|null
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        if ($this->checkIfTableExists($table, true, $dbName) && count($params) > 0 && count($conditions) > 0) {
            $this->reset();
            $query = 'UPDATE IGNORE ' . $dbNamePrefix . $table . ' SET ';
            $queryArray = [];
            $updateParams = [];
            foreach ($params as $key => $val) {
                if (self::checkCharactersTableAndFieldNames($key)) {
                    $queryArray[] = $key . '=?';
                    $updateParams[] = $val;
                } else {
                    return false;
                }
            }

            $query .= implode(',', $queryArray);
            $query .= ' WHERE ';
            $conditionsCount = 0;
            foreach ($conditions as $key => $val) {
                if (self::checkCharactersTableAndFieldNames($key)) {
                    if ($conditionsCount > 0) {
                        $query .= ' ' . $conjunction . ' ';
                    }

                    $updateParams[] = $val;
                    $query .= $key . '=?';
                    $conditionsCount++;
                } else {
                    return false;
                }
            }

            $dbQuery = $this->dataHost->prepare($query);
            $dbQuery->execute($updateParams);
            $this->affectedRows = $dbQuery->rowCount();

            return $this->affectedRows;
        } else {
            return false;
        }
    }

    /**
     * permanently delete a database record base on the primary key
     * @param string $table - table name
     * @param mixed $record -  the primary key record value
     * @param string|null $primaryKey
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Throwable
     */
    public function deleteRecord(string $table, mixed $record, ?string $primaryKey = NULL, ?string $dbName = null, bool $queryPreviewOnly = false): bool|string
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->checkIfTableExists($table, true, $dbName);
        $primaryKey = $primaryKey ?? $this->getPrimaryKey($table, true, false, $dbName);
        $v = $this->formatQueryParam($record);
        $params = [$record];
        $dbQuery = $this->dataHost->prepare("DELETE FROM $dbNamePrefix$table WHERE " . $primaryKey . '=?');
        $query = $dbQuery->queryString;
        $tQuery = $dbQuery->queryString;
        if ($queryPreviewOnly) {
            $tQuery = preg_replace('/\?/', $v, $tQuery, 1);
            $this->validateQueryParams($params, $query);

            return $tQuery;
        }

        $dbQuery->execute($params);

        return true;
    }

    /**
     * delete all records from a table
     * @param string $table - table name
     * @param bool $truncate
     * @param string|null $dbName
     * @return  bool
     * @throws Exception
     * @bool $truncate check if to delete the table and recreate it or just delete the records without recreating the table
     */
    public function emptyTable(string $table, bool $truncate = false, ?string $dbName = null): bool
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->checkIfTableExists($table, true, $dbName);
        if ($truncate) {
            $query = "TRUNCATE $dbNamePrefix$table";
        } else {
            $query = "DELETE FROM $dbNamePrefix$table WHERE 1";
        }

        $dbQuery = $this->dataHost->prepare($query);
        $dbQuery->execute();

        return true;
    }

    /**
     * delete table from the database if it exists
     * @param string $table - table name
     * @param bool $checkExists - check if table exists before dropping $checkExists
     * @param bool $dropForeignKeys - drop foreign keys that reference any column on the being dropped
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Throwable
     */
    public function dropTable(string $table, bool $checkExists = false, bool $dropForeignKeys = false, ?string $dbName = null, bool $queryPreviewOnly = false): bool|array
    {
        $resp = [];
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        if ($dropForeignKeys) {
            $foreignKeys = $this->getRows('SELECT DISTINCT TABLE_SCHEMA,TABLE_NAME, CONSTRAINT_NAME
            FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
            WHERE REFERENCED_TABLE_SCHEMA=? AND REFERENCED_TABLE_NAME=?', [$this->setDbName($dbName), $table]);
            if (count($foreignKeys) > 0) {
                foreach ($foreignKeys as $foreignKey) {
                    $fkDbNamePrefix = $this->setDbNamePrefix($foreignKey['TABLE_SCHEMA']);
                    $resp[] = $this->execute("ALTER TABLE $fkDbNamePrefix{$foreignKey['TABLE_NAME']} DROP FOREIGN KEY {$foreignKey['CONSTRAINT_NAME']}", [], $queryPreviewOnly);
                }
            }
        }

        if ($checkExists) {
            $this->checkIfTableExists($table, true, $dbName);
            $query = "DROP TABLE $dbNamePrefix$table";
        } else {
            $query = "DROP TABLE IF EXISTS $dbNamePrefix$table";
        }

        $dbQuery = $this->dataHost->prepare($query);
        if ($queryPreviewOnly) {
            $resp[] = $dbQuery->queryString;

            return $resp;
        }

        $dbQuery->execute();
        $this->getAllTableDetails($dbName);

        return true;
    }

    /**
     * return one record from the database based on the primary key
     * @param string $table - table name
     * @param mixed $record -  the primary key record value
     * @param string|null $primaryKey
     * @param string|null $dbName
     * @return object|array $this->row holds records if only one row is returned
     * @throws Exception
     */
    public function getRecord(string $table, mixed $record, ?string $primaryKey = NULL, ?string $dbName = null): object|array
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->reset();
        $this->checkIfTableExists($table, true, $dbName);
        $primaryKey = $primaryKey ?? $this->getPrimaryKey($table, true, false, $dbName);
        $params = [':' . $primaryKey => $record];
        $query = "SELECT * FROM $dbNamePrefix$table WHERE " . $primaryKey . '=:' . $primaryKey;
        $dbQuery = $this->dataHost->prepare($query);
        $dbQuery->execute($params);
        $this->result = $dbQuery->fetchAll(PDO::FETCH_ASSOC);
        $this->count = count($this->result);
        if ($this->count != 1) {
            throw new Exception("No $table Records Found", 404);
        }

        foreach ($this->result as $row) {
            $this->row = $row;
        }

        return $this->row;
    }

    /**
     * put a given array data in a HTML table string
     * @param array $result
     * @return string|null
     */
    public function echoTable(array $result = []): ?string
    {
        if (count($result) == 0) {
            $result = $this->result;
        }

        $headerYet = '';
        $echo = '<table border="1" style="border-color:#EEE">';
        if (is_array($result)) {
            if (count($result) > 0) {
                foreach ($result as $row) {
                    if (!$headerYet) {
                        $headerYet = 'yes';
                        $echo .= '<tr>';
                        foreach ($row as $key => $val) {
                            $echo .= '<th>' . $key . '</th>';
                        }

                        $echo .= '</tr>';
                    }
                    $echo .= '<tr class="all">';
                    foreach ($row as $val) {
                        $echo .= '<td>' . $val . '</td>';
                    }

                    $echo .= '</tr>';
                }
                $echo .= '</table>';

                return $echo;
            } else {
                return 'No results';
            }
        }

        return null;
    }

    /**
     * create a database table
     * @param string $table - table name
     * @param string $primaryKey - the name of the primary key column
     * @param array $fields -  the columns to be set
     * @param bool $withTimestamps add created_at and updated_at timestamps to the table
     * @param array $uniqueFieldPairs - unique field pairs eg [["column1,"column2"], ["column3","column4","column1"]]
     * @param bool $ignoreIfTableExists
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Throwable
     */
    public function createTable(string $table, string $primaryKey = 'id', array $fields = [], bool $withTimestamps = false, array $uniqueFieldPairs = [], bool $ignoreIfTableExists = true, ?string $dbName = null, bool $queryPreviewOnly = false): bool|string
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $tTable = $dbNamePrefix . $table;
        if (!$ignoreIfTableExists && $this->checkIfTableExists($table, false, $dbName)) {
            throw new Exception('Table ' . $table . ' already exists', 400);
        }

        $pkStr = "$primaryKey BIGINT UNSIGNED NOT NULL AUTO_INCREMENT %s COMMENT 'table unique identifier'";
        $createdAtStr = "created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When Record was created'";
        $updatedAtStr = "updated_at TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'When Record was updated'";
        self::checkCharactersTableAndFieldNames($primaryKey);
        if ($this->checkIfTableExists($table, false, $dbName)) {
            $tQuery = "ALTER TABLE $tTable ";
            if ($this->checkIfColumnInTableExists($table, $primaryKey, false, $dbName)) {
                $tQuery .= sprintf("MODIFY $pkStr", '');
            } else {
                $tQuery .= sprintf("ADD $pkStr", '');
            }

            $pk = $this->getPrimaryKey($table, false, true, $dbName);
            if (empty($pk)) {
                $tQuery .= ' PRIMARY KEY';
            } elseif ($pk !== $primaryKey) {
                throw new Exception('A different Primary Key ' . $pk . ' already exists');
            }

            $columns = $this->prepareAddColumnsSubQuery($table, $tQuery, $fields, 1, ['existing_table' => true, 'check_column_exists' => true], $dbName);
            $query = $columns['query'];
            $params = $columns['params'];
            $initialFieldsCount = $columns['query_fields'];
            if ($withTimestamps) {
                $query .= ',';
                if ($this->checkIfColumnInTableExists($table, 'created_at', false, $dbName)) {
                    $query .= 'MODIFY';
                } else {
                    $query .= 'ADD';
                }

                $query .= " COLUMN $createdAtStr,";
                if ($this->checkIfColumnInTableExists($table, $primaryKey, false, $dbName)) {
                    $query .= 'MODIFY';
                } else {
                    $query .= 'ADD';
                }

                $query .= " COLUMN $updatedAtStr";
                $initialFieldsCount++;
            }

            $this->addUniqueKeysSubQuery($query, $uniqueFieldPairs, $initialFieldsCount, $table, $dbName); //add unique keys
        } else {
            $tQuery = sprintf("CREATE TABLE $tTable ($pkStr", 'PRIMARY KEY');
            $columns = $this->prepareAddColumnsSubQuery($table, $tQuery, $fields, 1, [], $dbName);
            $query = $columns['query'];
            $params = $columns['params'];
            $initialFieldsCount = $columns['query_fields'];
            if ($withTimestamps) {
                $query .= ", $createdAtStr, $updatedAtStr";
                $initialFieldsCount++;
            }

            $this->addUniqueKeysSubQuery($query, $uniqueFieldPairs, $initialFieldsCount); //add unique keys
            $query .= ')';
        }

        $dbQuery = $this->dataHost->prepare($query);
        $query = $dbQuery->queryString;
        $tQuery = $dbQuery->queryString;
        foreach ($params as &$value) {
            $v = $this->formatQueryParam($value);
            $tQuery = preg_replace('/\?/', $v, $tQuery, 1);
        }

        if ($queryPreviewOnly) {
            $this->validateQueryParams($params, $query);

            return $tQuery;
        }

        $dbQuery->execute($params);

        return true;
    }

    /**
     * Renames table
     * @param string $oldTable - current table name
     * @param string $newTable - new table name
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Exception
     */
    public function renameTable(string $oldTable, string $newTable, ?string $dbName = null, bool $queryPreviewOnly = false): bool|string
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->checkIfTableExists($oldTable, true, $dbName);
        self::checkCharactersTableAndFieldNames($newTable);
        $query = "ALTER TABLE $dbNamePrefix$oldTable  RENAME TO $dbNamePrefix$newTable";
        $dbQuery = $this->dataHost->prepare($query);
        if ($queryPreviewOnly) {
            return $dbQuery->queryString;
        }

        $dbQuery->execute();

        return true;
    }

    /**
     * Renames a table column and all column indexes
     * @param string $table - The table name for the column to be renamed
     * @param string $oldColumn - current column name
     * @param string $newColumn - new column name
     * @param bool $renameIndexes - whether to rename the column indexes oe not
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|array
     * @throws Exception
     */
    public function renameColumn(string $table, string $oldColumn, string $newColumn, bool $renameIndexes = false, ?string $dbName = null, bool $queryPreviewOnly = false): bool|array
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $resp[] = $this->execute("ALTER TABLE $dbNamePrefix$table RENAME COLUMN $oldColumn TO $newColumn", [], $queryPreviewOnly);
        if ($renameIndexes) {
            $indexes = $this->getRows('SELECT DISTINCT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA=? AND TABLE_NAME=? AND COLUMN_NAME=? AND INDEX_NAME LIKE ?', [
                $this->setDbName($dbName), $table, $oldColumn, "%$oldColumn%"
            ]);
            foreach ($indexes as $index) {
                $newIndex = str_replace($index, $newColumn, $oldColumn);
                $resp[] = $this->execute("ALTER TABLE $dbNamePrefix$table RENAME INDEX $index TO $newIndex", [], $queryPreviewOnly);
            }
        }

        return $queryPreviewOnly ? $resp : true;
    }

    /**
     * Add column to an existing table
     * @param string $table - table name
     * @param array $fields -  the columns to be added
     * @param array $uniqueFieldPairs
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Throwable
     */
    public function addColumns(string $table, array $fields, array $uniqueFieldPairs = [], ?string $dbName = null, bool $queryPreviewOnly = false): bool|string
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        if (count($fields) > 0 || count($uniqueFieldPairs) > 0) {
            $this->checkIfTableExists($table, true, $dbName);
            $tQuery = "ALTER TABLE $dbNamePrefix$table";
            $columns = $this->prepareAddColumnsSubQuery($table, $tQuery, $fields, 0, ['existing_table' => true], $dbName);
            $query = $columns['query'];
            $params = $columns['params'];
            $this->addUniqueKeysSubQuery($query, $uniqueFieldPairs, $columns['query_fields'], $table, $dbName); //add unique keys
            $dbQuery = $this->dataHost->prepare($query);
            $query = $dbQuery->queryString;
            $tQuery = $dbQuery->queryString;
            foreach ($params as &$value) {
                $v = $this->formatQueryParam($value);
                $tQuery = preg_replace('/\?/', $v, $tQuery, 1);
            }

            if ($queryPreviewOnly) {
                $this->validateQueryParams($params, $query);

                return $tQuery;
            }

            $dbQuery->execute($params);
        }

        return true;
    }

    /**
     * drop multiple or one column(s) from an existing table
     * @param string $table - table name
     * @param array $columnNames - list of columns to be dropped
     * @param bool $dropForeignKeys - drop foreign keys that reference the column(s) being dropped
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|array
     * @throws Exception
     */
    public function dropColumns(string $table, array $columnNames, bool $dropForeignKeys = false, ?string $dbName = null, bool $queryPreviewOnly = false): bool|array
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->checkIfTableExists($table, true, $dbName);
        foreach ($columnNames as $columnName) {
            $this->checkIfColumnInTableExists($table, $columnName, true, $dbName);
        }

        $resp = [];
        if ($dropForeignKeys) {
            $whereColumn = '(?' . str_repeat(',?', count($columnNames) - 1) . ')';
            $foreignKeys = $this->getRows("SELECT DISTINCT TABLE_SCHEMA,TABLE_NAME, CONSTRAINT_NAME
            FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
            WHERE REFERENCED_TABLE_SCHEMA=? AND 
                (REFERENCED_TABLE_NAME=? AND REFERENCED_COLUMN_NAME IN $whereColumn) OR 
                (REFERENCED_TABLE_NAME IS NOT NULL AND REFERENCED_COLUMN_NAME IS NOT NULL AND TABLE_NAME=? AND COLUMN_NAME IN $whereColumn)",
                array_merge([$this->setDbName($dbName), $table], $columnNames, [$table], $columnNames));
            if (count($foreignKeys) > 0) {
                foreach ($foreignKeys as $foreignKey) {
                    $fkDbNamePrefix = $this->setDbNamePrefix($foreignKey['TABLE_SCHEMA']);
                    $resp[] = $this->execute("ALTER TABLE $fkDbNamePrefix{$foreignKey['TABLE_NAME']} DROP FOREIGN KEY {$foreignKey['CONSTRAINT_NAME']}", [], $queryPreviewOnly);
                }
            }
        }

        $resp[] = $this->execute("ALTER TABLE $dbNamePrefix$table DROP COLUMN " . implode(',', $columnNames), [], $queryPreviewOnly);

        return $queryPreviewOnly ? $resp : true;
    }

    /**
     * change properties of an existing column in a table
     * @param string $table - table name
     * @param array $fields -  the columns to be modified
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Throwable
     */
    public function modifyColumn(string $table, array $fields, ?string $dbName = null, bool $queryPreviewOnly = false): bool|string
    {
        $dbNamePrefix = $this->setDbNamePrefix($dbName);
        $this->checkIfTableExists($table, true, $dbName);
        $tQuery = "ALTER TABLE $dbNamePrefix$table";
        $columns = $this->prepareAddColumnsSubQuery($table, $tQuery, $fields, 0, ['existing_table' => true, 'modify_column' => true], $dbName);
        $query = $columns['query'];
        $params = $columns['params'];
        $dbQuery = $this->dataHost->prepare($query);
        $query = $dbQuery->queryString;
        $tQuery = $dbQuery->queryString;
        foreach ($params as &$value) {
            $v = $this->formatQueryParam($value);
            $tQuery = preg_replace('/\?/', $v, $tQuery, 1);
        }

        if ($queryPreviewOnly) {
            $this->validateQueryParams($params, $query);

            return $tQuery;
        }

        $dbQuery->execute($params);

        return true;
    }

    /**
     * Creates an order by string
     * @param string $table
     * @param array $orderBy - @format {"column_name1":"asc", "column_name2":"desc"}
     * @param string|null $dbName
     * @return string
     * @throws Exception
     */
    public function setOrderBy(string $table, array $orderBy = [], ?string $dbName = null): string
    {
        $orderByStr = '';
        if (count($orderBy) > 0) {
            if (count(array_diff(array_map('strtolower', $orderBy), ['asc', 'desc'])) > 0) {
                throw new Exception('Invalid sort direction', 403);
            }

            $orderByStr .= 'ORDER BY ';
            $allColumns = $this->getColumns($table, false, $dbName) ?? [];
            foreach ($orderBy as $column => $direction) {
                if (!in_array($column, $allColumns)) {
                    throw new Exception('Invalid Column', 403);
                }

                $orderByStr .= $column . ' ' . $direction . ' ';
            }
        }

        return $orderByStr;
    }

    /**
     * sets where filters
     * @param array $filter @format "filters": {"$and": [{"field_name": "id","operator": "$btwn","value": [1,6]},{"$parenthesis": {"$or": [{"$parenthesis": {"$or": [{"field_name": "city","operator": "$bgw","value": "nai"},{"field_name": "country","operator": "$in","value": ["india","kenya","algeria","usa"]}]}}]}}]}
     * @param array $fieldNameReplace @format {"data":{"field_id1":"field_name1","field_id2":"field_name2"},"filter_field_key"=>"field_id1"}
     * @param array $queryParams used when filter type is $query_param @format {"query_param1":123,"query_param2":"name"}
     * @param array $namedQueryParams
     * @return array
     * @throws Exception
     */
    public function setFilters(array $filter = [], array $fieldNameReplace = [], array $queryParams = [], array $namedQueryParams = []): array
    {
        $totalFilters = count($filter);
        if ($totalFilters > 0) {
            $validConjunctions = array_keys(self::$searchConjunctions);
            $validOperators = array_keys(self::$searchOperatorsStart);
            $validNullableOperators = array_keys(self::$searchNullableOperators);
            if (empty($fieldNameReplace['filter_field_key'])) {
                $totalReplacement = 0;
            } else {
                $totalReplacement = count($fieldNameReplace['data'] ?? []);
            }

            $conjunctionPattern = '/( or | and )$/i';

            return $this->recursiveFilters($filter, $queryParams, $namedQueryParams, [], $validConjunctions, $validOperators, $validNullableOperators, true, $totalFilters > 1, $fieldNameReplace, $totalReplacement, $conjunctionPattern);
        } else {
            return [];
        }
    }

    /**
     * @param string $table
     * @param string $columnName
     * @param bool $exception
     * @param string|null $dbName
     * @return string|null
     * @throws Exception
     */
    public function getColumnType(string $table, string $columnName, bool $exception = true, ?string $dbName = null): ?string
    {
        $columnType = $this->getRows('SELECT COLUMN_TYPE AS column_type FROM information_schema.columns WHERE
                table_name = ? AND column_name = ? AND TABLE_SCHEMA=?', [$table, $columnName, $this->setDbName($dbName)])[0]['column_type'] ?? null;
        if (empty($columnType) && $exception) {
            throw new Exception('Column Type Unknown', 400);
        }

        return $columnType;
    }

    /**
     * @throws Exception
     */
    public static function setFilterPrefix(array $prefix, array $dbFields, array $dbTables): string
    {
        if (empty($prefix['type'])) {
            throw new Exception('Prefix type must be provided', 400);
        }

        switch ($prefix['type']) {
            case '$table':
                $tableName = $prefix['table_name'] ?? $dbTables[$prefix['table_id']]['table_name'] ?? $dbFields['table_name'] ?? 0;
                if (empty($tableName)) {
                    throw new Exception('table_name|table_id for prefix type $table must be provided', 400);
                }

                $prefixName = $tableName;
                break;
            case '$join':
                if (!isset($prefix['join_name'])) {
                    throw new Exception('join_name for type $join must be provided', 400);
                }

                $prefixName = $prefix['join_name'];
                break;
            case '$sub_query':
                if (!isset($prefix['sub_query_name'])) {
                    throw new Exception('sub_query_name for type $sub_query must be provided', 400);
                }

                $prefixName = $prefix['sub_query_name'];
                break;
            default:
                throw new Exception('Invalid Prefix Type "' . $prefix['type'] . '"', 400);
        }

        self::checkCharactersTableAndFieldNames($prefixName);

        return "`$prefixName`.";
    }

    /**
     * @param array $function
     * @return string
     * @throws Exception
     */
    public static function setFieldFunctions(array $function): string
    {
        $f = $function['type'] ?? '';
        switch ($f) {
            case '$date_unit':
                $dateUnitTypeName = $function['date_unit_type'] ?? '';
                if (empty($dateUnitTypeName)) {
                    throw new Exception('date_unit_type for type $date_unit must be provided', 400);
                }

                if (!isset(Data::$dateUnits[$dateUnitTypeName])) {
                    throw new Exception('Invalid date_unit_type "' . $dateUnitTypeName . '"', 400);
                }

                $functionName = Data::$dateUnits[$dateUnitTypeName];
                break;
            default:
                throw new Exception('Invalid function type "' . $f . '"', 400);
        }

        return $functionName;
    }

    /**
     * @param string|null $dbName
     * @return string
     */
    public function setDbNamePrefix(?string $dbName = null): string
    {
        return $this->setDbName($dbName) . '.';
    }

    /**
     * @param string|null $dbName
     * @return string
     */
    public function setDbName(?string $dbName = null): string
    {
        return (empty($dbName) ? $this->dbName : $dbName);
    }

    /**
     * @param string $tableName
     * @param int $newValue
     * @param string|null $primaryKey
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Throwable
     */
    public function resetAutoIncrement(string $tableName, int $newValue = 1, ?string $primaryKey = NULL, ?string $dbName = null, bool $queryPreviewOnly = false): bool|string
    {
        $newValue = ($newValue <= 1) ? 0 : $newValue - 1;
        if (!$queryPreviewOnly || $this->checkIfTableExists($tableName, false, $dbName)) {
            $primaryKey = $primaryKey ?? $this->getPrimaryKey($tableName, true, true, $dbName);
            $maxValue = ($this->getRows("SELECT GREATEST(COALESCE(MAX($primaryKey)," . $newValue . '),' . $newValue . ") AS max_id FROM $tableName")[0]['max_id'] + 1);
            $resp = $this->execute("ALTER TABLE $tableName AUTO_INCREMENT = $maxValue", [], $queryPreviewOnly);
        } else {
            $resp = true;
        }

        return $queryPreviewOnly ? $resp : true;
    }

    /**
     * @param string $tableName
     * @param array $uniqueFieldPairs [['column1, column2],[column3]]
     * @param string|null $dbName
     * @param bool $queryPreviewOnly
     * @return bool|string
     * @throws Exception
     */
    public function addUniqueConstraints(string $tableName, array $uniqueFieldPairs, ?string $dbName = null, bool $queryPreviewOnly = false): bool|string
    {
        if (count($uniqueFieldPairs) > 0) {
            $query = "ALTER TABLE {$this->setDbName($dbName)}.$tableName ";
            if ($this->addUniqueKeysSubQuery($query, $uniqueFieldPairs, 0, $tableName, $dbName)) {
                $resp = $this->execute($query, [], $queryPreviewOnly);
                if ($queryPreviewOnly) {
                    return $resp;
                }
            }
        }

        return true;
    }

    /**
     * @param string $tableName
     * @param array $columnNames
     * @param bool $useTableNameOnly
     * @param string|null $dbName
     * @return bool
     * @throws Exception
     */
    public function checkUniqueKeyExists(string $tableName, array $columnNames, bool $useTableNameOnly = false, ?string $dbName = null): bool
    {
        $exists = false;
        $columnNames = array_values(array_unique(array_filter($columnNames)));
        $totalColumnNames = count($columnNames);
        if ($totalColumnNames > 0) {
            $dbName = $this->setDbName($dbName);
            $schemaTableName = $useTableNameOnly ? $tableName : $dbName . '.' . $tableName;
            $this->getRows("SELECT kcu.CONSTRAINT_NAME
                FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
                INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc ON kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME AND kcu.TABLE_SCHEMA=tc.TABLE_SCHEMA AND kcu.TABLE_NAME=tc.TABLE_NAME
                WHERE tc.CONSTRAINT_TYPE = 'UNIQUE' AND CONCAT_WS('.',tc.TABLE_SCHEMA,tc.TABLE_NAME) = ?
                GROUP BY kcu.CONSTRAINT_NAME HAVING COUNT(DISTINCT kcu.COLUMN_NAME)=$totalColumnNames AND SUM(kcu.COLUMN_NAME IN (?" . str_repeat(',?', $totalColumnNames - 1) . "))=$totalColumnNames", array_merge([$schemaTableName], $columnNames));
            $exists = ($this->count == 1);
        }

        return $exists;
    }

    /**
     * @param string $tableName
     * @param bool $useTableNameOnly If true, it assumes [table_schema.table_name] structure for $tableName
     * @param string|null $dbName
     * @return array
     * @throws Exception
     */
    public function getTableUniqueKeys(string $tableName, bool $useTableNameOnly = false, ?string $dbName = null): array
    {
        $dbName = $this->setDbName($dbName);
        $schemaTableName = $useTableNameOnly ? $tableName : $dbName . '.' . $tableName;
        return $this->getRows("SELECT kcu.CONSTRAINT_NAME,JSON_ARRAYAGG(kcu.COLUMN_NAME) AS COLUMN_NAME
                FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
                INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc ON kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME AND kcu.TABLE_SCHEMA=tc.TABLE_SCHEMA AND kcu.TABLE_NAME=tc.TABLE_NAME
                WHERE tc.CONSTRAINT_TYPE = 'UNIQUE' AND CONCAT_WS('.',tc.TABLE_SCHEMA,tc.TABLE_NAME) = ? 
                GROUP BY kcu.CONSTRAINT_NAME", [$schemaTableName], [$this::$jsonDecode => true]) ?? [];

    }

    /**
     * @param array $schemaTableNames
     * @return array
     * @throws Exception
     */
    public function getMultipleDatabaseUniqueKeys(array $schemaTableNames): array
    {
        $totalSchemaTableNames = count($schemaTableNames);
        if ($totalSchemaTableNames == 0) {
            return [];
        }

        return $this->getRows("SELECT CONCAT_WS('.', tc.TABLE_SCHEMA, tc.TABLE_NAME) AS TABLE_NAME,JSON_ARRAYAGG(kcu.COLUMN_NAME) AS COLUMN_NAME
                FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
                INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc ON kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME AND kcu.TABLE_SCHEMA=tc.TABLE_SCHEMA AND kcu.TABLE_NAME=tc.TABLE_NAME
                WHERE tc.CONSTRAINT_TYPE = 'UNIQUE' AND CONCAT_WS('.',tc.TABLE_SCHEMA,tc.TABLE_NAME) IN (?" . str_repeat(',?', $totalSchemaTableNames - 1) . ")
                GROUP BY CONCAT_WS('.', tc.TABLE_SCHEMA, tc.TABLE_NAME),kcu.CONSTRAINT_NAME", $schemaTableNames, [$this::$jsonDecode => true]) ?? [];

    }

    /**
     * @param string $tableName
     * @param bool $useTableNameOnly If true, it assumes [table_schema.table_name] structure for $tableName
     * @param string|null $dbName
     * @return array
     * @throws Exception
     * https://dev.mysql.com/doc/refman/8.0/en/create-index.html
     */
    public function getTableIndexes(string $tableName, bool $useTableNameOnly = false, ?string $dbName = null): array
    {
        $dbName = $this->setDbName($dbName);
        $schemaTableName = $useTableNameOnly ? $tableName : $dbName . '.' . $tableName;
        return $this->getRows("SELECT CONCAT_WS('.',TABLE_SCHEMA,TABLE_NAME) AS TABLE_NAME, JSON_ARRAYAGG(COLUMN_NAME) AS COLUMN_NAME,INDEX_TYPE,CASE WHEN INDEX_TYPE = 'BTREE' THEN 'INDEX %s' WHEN INDEX_TYPE = 'FULLTEXT' THEN 'FULLTEXT INDEX %s' WHEN INDEX_TYPE IN ('SPATIAL','RTREE') THEN 'SPATIAL INDEX %s' WHEN INDEX_TYPE = 'HASH' THEN 'INDEX %s USING HASH' ELSE CONCAT_WS(' ',INDEX_TYPE,'%s') END AS HUMAN_READABLE_INDEX_TYPE
            FROM INFORMATION_SCHEMA.STATISTICS 
            WHERE CONCAT_WS('.',TABLE_SCHEMA,TABLE_NAME)=? AND NON_UNIQUE=true 
            GROUP BY CONCAT_WS('.',TABLE_SCHEMA,TABLE_NAME),INDEX_NAME,INDEX_TYPE", [$schemaTableName], [$this::$jsonDecode => true]) ?? [];
    }

    /**
     * @param array $schemaTableNames [table_schema.table_name]
     * @return array
     * @throws Exception
     *  https://dev.mysql.com/doc/refman/8.0/en/create-index.html
     */
    public function getMultipleDatabaseIndexes(array $schemaTableNames): array
    {
        $totalSchemaTableNames = count($schemaTableNames);
        if ($totalSchemaTableNames == 0) {
            return [];
        }

        return $this->getRows("SELECT CONCAT_WS('.',TABLE_SCHEMA,TABLE_NAME) AS TABLE_NAME,INDEX_TYPE, JSON_ARRAYAGG(COLUMN_NAME) AS COLUMN_NAME,CASE WHEN INDEX_TYPE = 'BTREE' THEN 'INDEX %s' WHEN INDEX_TYPE = 'FULLTEXT' THEN 'FULLTEXT INDEX %s' WHEN INDEX_TYPE IN ('SPATIAL','RTREE') THEN 'SPATIAL INDEX %s' WHEN INDEX_TYPE = 'HASH' THEN 'INDEX %s USING HASH' ELSE CONCAT_WS(' ',INDEX_TYPE,'%s') END AS HUMAN_READABLE_INDEX_TYPE
            FROM INFORMATION_SCHEMA.STATISTICS 
            WHERE CONCAT_WS('.',TABLE_SCHEMA,TABLE_NAME) IN (?" . str_repeat(',?', $totalSchemaTableNames - 1) . ") AND NON_UNIQUE=true 
            GROUP BY CONCAT_WS('.',TABLE_SCHEMA,TABLE_NAME),INDEX_NAME,INDEX_TYPE", $schemaTableNames, [$this::$jsonDecode => true]) ?? [];

    }

    /**
     * @param string $tableName
     * @param string $columnName
     * @param string $referencedDb
     * @param string $referencedTable
     * @param string $referencedColumn
     * @param string $onDeleteAction
     * @param string $onUpdateAction
     * @param bool $tableExists
     * @param bool $prependComma
     * @param string|null $dbName
     * @return string
     * @throws Exception
     */
    public function prepareAddFkSubquery(string $tableName, string $columnName, string $referencedDb, string $referencedTable, string $referencedColumn, string $onDeleteAction, string $onUpdateAction, bool $tableExists = false, bool $prependComma = false, ?string $dbName = null): string
    {
        $query = '';
        $dbName = $this->setDbName($dbName);
        $constraint = '';
        if (strlen($tableName . '_ibfk_nn') > self::setIdentifierMaxLength()['Constraint']) {
            $constraint = 'CONSTRAINT `' . substr($tableName, 0, 35) . '..._ibfk_' . time() . '` ';
        }

        $addFkStr = " {$constraint}FOREIGN KEY ($columnName) REFERENCES $referencedDb.$referencedTable($referencedColumn)";
        $fk = $addFkStr;
        if ($tableExists) {
            $addFkStr = ' ADD' . $addFkStr;
            $fkConstraint = $this->getRows('SELECT kcu.CONSTRAINT_NAME,kcu.REFERENCED_TABLE_SCHEMA,kcu.REFERENCED_TABLE_NAME,kcu.REFERENCED_COLUMN_NAME,rc.DELETE_RULE,rc.UPDATE_RULE  
                    FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu 
                    INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc ON rc.CONSTRAINT_NAME = kcu.CONSTRAINT_NAME 
                    WHERE kcu.TABLE_SCHEMA = ? AND kcu.TABLE_NAME = ? AND kcu.COLUMN_NAME = ? AND kcu.REFERENCED_TABLE_NAME IS NOT NULL', [$dbName, $tableName, $columnName])[0] ?? [];
            if (empty($fkConstraint)) {
                $fk = $addFkStr;
            } elseif ($fkConstraint['REFERENCED_TABLE_SCHEMA'] == $referencedDb && $fkConstraint['REFERENCED_TABLE_NAME'] == $referencedTable &&
                $fkConstraint['REFERENCED_COLUMN_NAME'] == $referencedColumn && !empty($onDeleteAction) && strtoupper($onDeleteAction) == $fkConstraint['DELETE_RULE'] && !empty($onUpdateAction) && strtoupper($onUpdateAction) == $fkConstraint['UPDATE_RULE']) {
                $fk = '';
            } else {
                $fk = "DROP FOREIGN KEY {$fkConstraint['CONSTRAINT_NAME']} ,$addFkStr";
            }
        }

        if (!empty($fk)) {
            if ($prependComma) {
                $query = ',';
            }

            $query .= $fk;
            if (!empty($onDeleteAction)) {
                $query .= " ON DELETE $onDeleteAction";
            }

            if (!empty($onUpdateAction)) {
                $query .= " ON UPDATE $onUpdateAction";
            }
        }

        return $query;
    }

    /**
     * Validates the length of an identifier
     * @param string $identifier
     * @param string $identifier_type
     * @param bool $ignoreIdentifierInErrorMessage
     * @throws Exception
     */
    public static function validateIdentifier(string $identifier, string $identifier_type, bool $ignoreIdentifierInErrorMessage = false): void
    {
        $max_lengths = self::setIdentifierMaxLength();
        if (!empty($max_lengths[$identifier_type]) && strlen($identifier) > $max_lengths[$identifier_type]) {
            $identifier = $ignoreIdentifierInErrorMessage ? '' : " $identifier";
            throw new Exception("$identifier_type$identifier exceeds maximum length size of $max_lengths[$identifier_type]" . ($identifier_type == self::$pivotTableIdentifier ? ' use custom pivot table name instead' : ''), 400);
        }
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     * @return string
     */
    public function setLimitAndOffset(?int $limit, ?int $offset): string
    {
        $tOffset = '';
        if (empty($offset)) {
            $limitStr = empty($limit) ? '' : " LIMIT  $limit";
        } else {
            $tOffset = " OFFSET $offset";
            $limitStr = empty($limit) ? ' LIMIT ' . PHP_INT_MAX : " LIMIT $limit";
        }

        return $limitStr . $tOffset;
    }

    /**
     * @param string $table - table name
     * @param array $params - parameters used in preparing the valid columns array
     * @param string|null $dbName
     * @return array
     * @throws Exception
     */
    private function setUpsertFields(string $table, array $params, ?string $dbName = null): array
    {
        $columns = $this->getColumns($table, false, $dbName);
        $return = array_filter($params, function ($column) use ($columns) {
            return in_array($column, $columns);
        }, ARRAY_FILTER_USE_KEY);

        if (count($return) == 0) {
            throw new Exception('No valid columns', 400);
        }

        return $return;
    }

    /**
     * @param mixed $data
     * @param callable|null $callback
     * @param bool $filterDecodedArray - filters decoded null array eg {"key1":null,"key2":null} to {}
     * @param array $decodeExclusiveArrayKeys - field keys to be excluded from decoding
     * @return void
     */
    public static function json_decode_recursively(mixed &$data, ?callable $callback = NULL, bool $filterDecodedArray = false, array $decodeExclusiveArrayKeys = []): void
    {
        if (is_array($data)) {
            array_walk($data, function (&$value, $key) use ($filterDecodedArray, $callback, $decodeExclusiveArrayKeys) {
                if (in_array($key, $decodeExclusiveArrayKeys)) {
                    return;
                }

                self::json_decode_recursively($value, $callback, $filterDecodedArray, $decodeExclusiveArrayKeys);
            });
        } elseif (is_string($data)) {
            $val = json_decode($data, true, 512, JSON_BIGINT_AS_STRING);
            if (json_last_error() === JSON_ERROR_NONE) {
                if (is_array($val)) {
                    self::json_decode_recursively($val, $callback, $filterDecodedArray, $decodeExclusiveArrayKeys);
                } elseif (is_float($val) && is_infinite($val)) {
                    $val = $data;
                }

                $data = (is_array($val) && $filterDecodedArray) ? array_filter($val) : $val;
            }
        }
    }

    /**
     * Recurse nested filters
     * @param array $filter
     * @param array $queryParams
     * @param array $namedQueryParams
     * @param array $bindings
     * @param array $validConjunctions
     * @param array $validOperators
     * @param array $validNullableOperators
     * @param bool $isParent
     * @param bool $enclose
     * @param array $fieldNameReplace
     * @param int $totalReplacement
     * @param string $conjunctionPattern
     * @return array
     * @throws Exception if filter is has is_json_key set to true, the json_key should be dot-separated example:
     * if the field is {"key1":{"nested1":"value"}},and you filter by nested1, the json_key should be key1.nested1
     */
    private function recursiveFilters(array $filter, array $queryParams, array $namedQueryParams, array $bindings, array $validConjunctions, array $validOperators, array $validNullableOperators, bool $isParent = false, bool $enclose = FALSE, array $fieldNameReplace = [], int $totalReplacement = 0, string $conjunctionPattern = ''): array
    {
        $query = '';
        $i = 0;
        foreach ($filter as $conjunction => $value) {
            if (!in_array($conjunction, $validConjunctions)) {
                throw new Exception('Invalid conjunction', 1);
            }

            if ($isParent && $i++ > 0) {
                $query .= ' AND ';
            }

            $close = '';
            if ($enclose) {
                $query .= '(';
                $close = ')';
            }

            $j = 0;
            $valueCount = count($value);
            foreach ($value as $data) {
                $data['type'] = $data['type'] ?? '';
                if (isset($data['$parenthesis'])) {
                    $tQuery = $this->recursiveFilters($data['$parenthesis'], $queryParams, $namedQueryParams, $bindings, $validConjunctions, $validOperators, $validNullableOperators, false, FALSE, $fieldNameReplace, $totalReplacement, $conjunctionPattern);
                    if (is_array($tQuery['query'])) {
                        $tQuery = implode('', $tQuery['query']);
                    }

                    if (empty($tQuery['query']) || in_array($tQuery['query'], self::$searchConjunctions)) {
                        $query = preg_replace($conjunctionPattern, '', $query);
                    } else {
                        $tQuery['query'] = preg_replace($conjunctionPattern, '', $tQuery['query']);
                        $query .= '(';
                        $bindings = $tQuery['params'];
                        $query .= $tQuery['query'];
                        $query .= ') ';
                    }
                } else {
                    if (array_key_exists('required', $data) && $data['required'] === false) {
                        if (in_array($data['operator'], self::$searchNullableOperators)) {
                            if ($data['type'] == '$query_param') {
                                if (empty($queryParams[$data['value']])) {
                                    continue;
                                }
                            } elseif (array_key_exists('value', $data) && $data['value'] === false) {
                                continue;
                            }
                        } elseif ($data['type'] == '$query_param') {
                            if (is_array($data['value'])) {
                                if (count(array_diff($data['value'], array_keys($queryParams))) >= count($data['value'])) {
                                    continue;
                                }

                                $data['value'] = array_keys(array_intersect_key($queryParams, array_flip($data['value'])));
                            } elseif (!array_key_exists($data['value'], $queryParams)) {
                                continue;
                            }
                        } elseif (!array_key_exists('value', $data)) {
                            continue;
                        }
                    }

                    $tData = (empty($fieldNameReplace['filter_field_key']) ? NULL : ($fieldNameReplace['data'][$data[$fieldNameReplace['filter_field_key']]] ?? NULL));
                    if ($totalReplacement > 0 && !empty($tData)) {
                        if (is_array($tData)) {
                            $fName = '';
                            if (!empty($data['prefix'] && is_array($data['prefix']))) {
                                $fName = self::setFilterPrefix($data['prefix'], $tData, $fieldNameReplace['tables'] ?? []);
                            }

                            $fName .= $data['field_name'] ?? $tData['field_name'] ?? NULL;

                        } else {
                            $fName = $tData;
                        }

                        $data['field_name'] = $fName;

                    } elseif (!empty($data['field_name']) && !empty($data['prefix']) && is_array($data['prefix'])) {
                        $data['field_name'] = self::setFilterPrefix($data['prefix'], [], $fieldNameReplace['tables'] ?? []) . $data['field_name'];
                    }


                    if (!isset($data['field_name']) || !isset($data['operator']) || (!in_array($data['operator'], self::$searchNullableOperators) && !isset($data['value']))) {

                        throw new Exception('field_name/operator/value is missing', 1);
                    }

                    if (!in_array($data['operator'], $validOperators)) {
                        throw new Exception('Invalid operator', 1);
                    }

                    $fieldName = $data['field_name'];
                    $operator = self::$searchOperatorsStart[$data['operator']];
                    if (!empty($data['is_json_key']) && is_bool($data['is_json_key'])) {
                        if (!empty($data['json_key'])) {
                            $fieldName = "JSON_VALUE($fieldName, '$.{$data['json_key']}')";
                        }
                    }

                    if (!empty($data['function'])) {
                        $fieldName = self::setFieldFunctions($data['function']) . "($fieldName)";
                    }

                    if (!empty($data['named_field_keys']) && is_array($data['named_field_keys'])) {
                        foreach ($data['named_field_keys'] as $namedFieldKey) {
                            $bindings[] = $namedQueryParams[$namedFieldKey] ?? '';
                            $fieldName = str_replace($namedFieldKey, '?', $fieldName);
                        }
                    }

                    $initQuery = $query;
                    $query .= $fieldName . $operator;
                    if ($data['type'] == '$mysql') {
                        if (!empty($data['named_value_keys']) && is_array($data['named_value_keys'])) {
                            foreach ($data['named_value_keys'] as $namedValueKey) {
                                $bindings[] = $namedQueryParams[$namedValueKey] ?? '';
                                $data['value'] = str_replace($namedValueKey, '?', $data['value']);
                            }
                        }

                        $query .= $data['value'];
                    } elseif ($data['type'] == '$sub_query') {
                        if (!is_string($data['value'])) {
                            throw new Exception('Invalid value', 400);
                        }

                        if (in_array($data['operator'], self::$searchNullableOperators)) {
                            throw new Exception('Invalid operator for sub_query', 400);
                        }

                        if ($data['params']) {
                            $bindings = array_merge($bindings, $data['params']);
                        }

                        $query .= "({$data['value']})";

                    } elseif (!in_array($data['operator'], self::$searchNullableOperators)) {
                        if (in_array($data['operator'], ['$in', '$nin'])) {
                            if (!is_array($data['value'])) {
                                throw new Exception('Invalid value', 400);
                            }

                            $op = '';
                            foreach ($data['value'] as $v) {
                                if (is_array($v)) {
                                    throw new Exception('Invalid value', 400);
                                }

                                if ($data['type'] == '$query_param') {
                                    if (!array_key_exists($v, $queryParams)) {
                                        throw new Exception('Missing query param ' . $v, 400);
                                    }

                                    $v = $queryParams[$v];
                                    if (is_array($v)) {
                                        throw new Exception('Invalid value', 400);
                                    }
                                }

                                $bindings[] = $v;
                                $op .= '?,';
                            }
                            $query .= trim($op, ',');
                        } elseif (in_array($data['operator'], ['$btwn', '$nbtwn'])) {
                            if (!is_array($data['value']) || count($data['value']) != 2) {
                                throw new Exception('Invalid value', 400);
                            } else {
                                $query .= '? AND ?';
                                foreach ($data['value'] as $v) {
                                    if (is_array($v)) {
                                        throw new Exception('Invalid value', 400);
                                    }

                                    if ($data['type'] == '$query_param') {
                                        if (!array_key_exists($v, $queryParams)) {
                                            throw new Exception('Missing query param ' . $v, 400);
                                        }

                                        $v = $queryParams[$v];
                                        if (is_array($v)) {
                                            throw new Exception('Invalid value', 400);
                                        }
                                    }

                                    $bindings[] = $v;
                                }
                            }
                        } elseif (is_array($data['value'])) {
                            throw new Exception('Invalid value', 400);
                        } else {
                            $upprcase = false;
                            if (in_array($data['operator'], ['$contains', '$ncontains', '$endw', '$nendw', '$bgw', '$nbgw'])) {
                                $query = $initQuery . "UPPER($fieldName)" . $operator;
                                $query .= 'UPPER(?)';
                                $upprcase = true;
                            } else {
                                $query .= '?';
                            }

                            $v = $data['value'];
                            if (is_array($v)) {
                                throw new Exception('Invalid value', 400);
                            }

                            if ($data['type'] == '$query_param') {
                                if (!array_key_exists($v, $queryParams)) {
                                    throw new Exception('Missing query param ' . $v, 400);
                                }

                                $v = $queryParams[$v];
                                if (is_array($v)) {
                                    throw new Exception('Invalid value', 400);
                                }
                            }

                            $bindings[] = ($upprcase ? strtoupper($v) : $v);
                        }
                    }

                    $query .= self::$searchOperatorsEnd[$data['operator']];
                }

                if (++$j < $valueCount) {
                    $query .= self::$searchConjunctions[$conjunction];
                }
            }

            $query .= $close;
        }

        if ($isParent) {
            $query = preg_replace($conjunctionPattern, '', $query);

        }

        return ['query' => $query, 'params' => $bindings];
    }

    /**
     * prepare add columns sub-query
     * @param string $tableName - table name
     * @param string $query - main query either alter table or create table
     * @param array $fields -  the columns to be set
     * @param int $initialFieldsCount
     * @param array $options - other options ['existing_table' => false, 'modify_column' => false]
     * @param string|null $dbName
     * @return array
     * @throws Exception
     * @throws Throwable
     */
    private function prepareAddColumnsSubQuery(string $tableName, string $query, array $fields = [], int $initialFieldsCount = 0, array $options = [], ?string $dbName = null): array
    {
        $params = [];
        if (count($fields) > 0) {
            foreach ($fields as $field) {
                if (!isset($field['name']) || !isset($field['type'])) {
                    throw new Exception('Missing Field Name or Type', 400);
                }

                self::checkCharactersTableAndFieldNames($field['name']);

                $columnSubQuery = '';
                $indexSubQuery = '';
                $tableExists = false;
                if (count($options) > 0) {
                    if (!empty($options['existing_table']) && is_bool($options['existing_table'])) {
                        $indexSubQuery = 'ADD';
                        $tableExists = true;
                        if (!empty($options['check_column_exists'])) {
                            $options['modify_column'] = $this->checkIfColumnInTableExists($tableName, $field['name'], false, $dbName);
                        }

                        if (!empty($options['modify_column']) && is_bool($options['modify_column'])) {
                            $this->checkIfColumnInTableExists($tableName, $field['name'], true, $dbName);
                            $columnSubQuery = 'MODIFY COLUMN';
                        } else {
                            if ($this->checkIfColumnInTableExists($tableName, $field['name'], false, $dbName)) {
                                throw new Exception("Column {$field['name']} already exists in table $tableName", 400);
                            }

                            $columnSubQuery = 'ADD COLUMN';
                        }
                    }
                }


                // creates foreign key "foreign_key" => ["table" => primary_table, "column" => "primary_id"]
                $foreignKeyConstraint = '';
                if (isset($field['foreign_key'])) {
                    if (is_array($field['foreign_key']) && isset($field['foreign_key']['table'])) {
                        $column = $field['foreign_key']['column'] ?? 'id';
                        $referencedDbName = empty($field['foreign_key']['database']) ? $this->dbName : "{$field['foreign_key']['database']}";
                        $field['type'] = $this->getColumnType($field['foreign_key']['table'], $column, false, $referencedDbName) ?? $field['type'];
                        $foreignKeyConstraint = $this->prepareAddFkSubquery($tableName, $field['name'], $referencedDbName, $field['foreign_key']['table'], $column, $field['foreign_key']['if_deleted'], $field['foreign_key']['if_updated'], $tableExists, true, $dbName);
                    } else {
                        throw new Exception('Foreign key requires referenced table', 1);
                    }
                }

                $type = $field['type'];
                if (is_array($type)) { //"type" => ["name" => "enum", "values" => "1234" ]
                    if (!isset($type['name']) || !isset($type['value']) || count($type['value']) == 0) {
                        throw new Exception('Invalid type', 400);
                    }

                    if ([$type['value']]) {
                        $params = array_merge($params, $type['value']);
                        $value = '?' . str_repeat(',?', count($type['value']) - 1);
                    } else {
                        $value = '?';
                        $params[] = $value;
                    }

                    $type = strtoupper($type['name']) . "($value)";
                } else {
                    $type = strtoupper($type);
                }

                if ($initialFieldsCount > 0) {
                    $query .= ',';
                }

                $fieldName = '`' . trim($field['name'], '`') . '`';
                $query .= " $columnSubQuery $fieldName " . $type;
                if (isset($field['length']) && is_numeric($field['length']) && $field['length'] > 0) {
                    $decimal = '';
                    if (isset($field['decimals']) && is_numeric($field['decimals']) && $field['decimals'] > 0) {
                        $decimal = ",{$field['decimals']}";
                        $field['length'] += $field['decimals'];
                    }

                    $query .= "({$field['length']}$decimal)";
                }

                if (!empty($field['is_auto_increment'])) {
                    $query .= ' AUTO_INCREMENT';
                }

                if (isset($field['generated_column']['expression'])) {
                    $query .= " GENERATED ALWAYS AS ({$field['generated_column']['expression']})";
                    if (!empty($field['generated_column']['is_stored']) && is_bool($field['generated_column']['is_stored'])) {
                        $query .= ' STORED';
                    } else {
                        $query .= ' VIRTUAL';
                    }
                }

                if (isset($field['is_nullable'])) {
                    if ($field['is_nullable'] === false) {
                        $query .= ' NOT NULL';
                    } else {
                        $query .= ' NULL';
                    }
                }

                if (isset($field['default'])) {
                    $query .= ' DEFAULT ?';
                    $params[] = $field['default'];
                }

                if (isset($field['description'])) {
                    $query .= ' COMMENT ?';
                    $params[] = $field['description'];
                }

                if (isset($field['after'])) {
                    $query .= " AFTER {$field['after']}";
                }

                if (isset($field['is_unique']) && $field['is_unique']) {
                    if (!$tableExists || !$this->checkUniqueKeyExists($tableName, [$field['name']], false, $dbName)) {
                        $query .= " ,$indexSubQuery UNIQUE KEY({$field['name']})";
                    }
                }

                $query .= $foreignKeyConstraint;
                if (!empty(is_string($field['index']) && $field['index'])) {
                    $indexes = [];
                    if ($tableExists) {
                        $indexes = $this->getTableIndexes($tableName, false, $dbName);
                    }

                    switch (preg_replace('/\s+/', '_', strtolower($field['index']))) {
                        case 'index':
                            if (!empty($indexes) && count(array_filter($indexes, function ($v) use ($field) {
                                    return count(array_diff(json_decode($v['COLUMN_NAME']), [$field['index']])) == 0 && $v['INDEX_TYPE'] == 'BTREE';
                                })) == 0) {
                                $query .= " ,$indexSubQuery INDEX({$field['name']}) COMMENT 'INDEX'";
                            }

                            break;
                        case 'primary_key':
                            if ($tableExists) {
                                $pk = $this->getPrimaryKey($tableName, false, false, $dbName);
                                if (empty($pk)) {
                                    $query .= " ,$indexSubQuery PRIMARY KEY({$field['name']}) COMMENT 'PRIMARY'";
                                } elseif ($pk !== $field['name']) {
                                    throw new Exception('A different Primary Key ' . $pk . ' already exists');
                                }
                            } else {
                                $query .= " ,$indexSubQuery PRIMARY KEY({$field['name']}) COMMENT 'PRIMARY'";
                            }

                            break;
                        case 'fulltext':
                            if (!empty($indexes) && count(array_filter($indexes, function ($v) use ($field) {
                                    return count(array_diff(json_decode($v['COLUMN_NAME']), [$field['index']])) == 0 && $v['INDEX_TYPE'] == 'FULLTEXT';
                                })) == 0) {
                                $query .= " ,$indexSubQuery FULLTEXT({$field['name']}) COMMENT 'FULLTEXT'";
                            }

                            break;
                    }
                }

                $initialFieldsCount++;
            }
        }

        return ['query' => $query, 'params' => $params, 'query_fields' => $initialFieldsCount];
    }

    /**
     * @param string $query
     * @param array $uniqueFieldPairs
     * @param int $initialFieldsCount
     * @param string|null $existingTableName
     * @param string|null $dbName
     * @return bool
     * @throws Exception
     */
    private function addUniqueKeysSubQuery(string &$query = '', array $uniqueFieldPairs = [], int $initialFieldsCount = 0, ?string $existingTableName = null, ?string $dbName = null): bool
    {
        $added = false;
        $uniquePairs = count($uniqueFieldPairs);
        if ($uniquePairs > 0) {
            if ($uniquePairs == count($uniqueFieldPairs, COUNT_RECURSIVE)) {
                throw new Exception('Invalid Unique field pairs', 400);
            }

            $queryArr = [];
            if (empty($existingTableName)) {
                foreach ($uniqueFieldPairs as $uniquePair) {
                    $queryArr[] = ' UNIQUE(' . implode(',', $uniquePair) . ')';
                }
            } else {
                foreach ($uniqueFieldPairs as $uniquePair) {
                    if (!$this->checkUniqueKeyExists($existingTableName, $uniquePair, false, $dbName)) {
                        $queryArr[] = ' ADD UNIQUE(' . implode(',', $uniquePair) . ')';
                    }
                }
            }

            if (count($queryArr) > 0) {
                $added = true;
                if ($initialFieldsCount > 0) {
                    $query .= ',';
                }

                $query .= implode(',', $queryArr);
            }
        }

        return $added;
    }

    /**
     * @param array $params
     * @param string $query
     * @return void
     * @throws Exception
     */
    private function validateQueryParams(array $params, string $query): void
    {
        $totalParams = count($params);
        preg_match_all('/:([a-zA-Z0-9_]+)|\?/', $query, $matches);
        $hasNamedParams = preg_match('/:([a-zA-Z0-9_]+)/', $query);
        $hasUnamedParams = preg_match('/\?/', $query);
        $expectedParams = $matches[0];
        $types = array_unique(array_map('gettype', array_keys($params)));
        $type = ($totalParams > 0) ? $types[0] : null;
        if (count($types) > 1 || ($hasUnamedParams && $hasNamedParams)) {
            throw new Exception('SQLSTATE[HY093]: Invalid parameter number: mixed named and positional parameters', 500);
        }

        if (!empty($type)) {
            $expectedParams = ($type == 'string') ? array_unique($expectedParams) : $expectedParams;
            if ($totalParams == 0 && (($type == 'string' && $hasUnamedParams) || ($type == 'integer' && $hasNamedParams))) {
                throw new Exception('SQLSTATE[HY093]: Invalid parameter number: parameter was not defined', 500);
            }
        }

        if (count($expectedParams) !== $totalParams) {
            throw new Exception('SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens', 500);
        }
    }

    /**
     * @param mixed $value
     * @return int|mixed|string
     */
    private function formatQueryParam(mixed &$value): mixed
    {
        if (is_bool($value)) {
            $v = $value ? 1 : 0;
            $value = $v;
        } elseif (is_array($value)) {
            $value = json_encode($value);
            $v = "'$value'";
        } elseif ($value == null) {
            $v = 'NULL';
        } elseif (is_string($value)) {
            $v = "'$value'";
        } else {
            $v = $value;
        }

        return $v;
    }

    /**
     * @param string $table
     * @param string|null $dbName
     * @return array
     * @throws Exception
     */
    private function getNotNullNoDefaultColumns(string $table, ?string $dbName = null): array
    {
        return array_column($this->getRows('SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ? AND UPPER(IS_NULLABLE) = ? AND COLUMN_DEFAULT IS NULL AND UPPER(EXTRA) !=?',
            [$this->setDbName($dbName), $table, 'NO', 'AUTO_INCREMENT']), 'COLUMN_NAME');
    }

    /**
     * @return int[]
     */
    private static function setIdentifierMaxLength(): array
    {
        return [
            self::$databaseIdentifier => 64,
            self::$tableIdentifier => 64,
            self::$pivotTableIdentifier => 64,
            self::$columnIdentifier => 64,
            self::$indexIdentifier => 64,
            self::$constraintIdentifier => 64,
            self::$storedProgramIdentifier => 64,
            self::$viewIdentifier => 64,
            self::$tablespaceIdentifier => 64,
            self::$serverIdentifier => 64,
            self::$logFileGroupIdentifier => 64,
            self::$aliasIdentifier => 256,
            self::$compoundStatementLabelIdentifier => 16,
            self::$userDefinedVariableIdentifier => 64,
            self::$resourceGroupIdentifier => 64
        ];
    }
}